﻿using Microsoft.AspNetCore.Http;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.GRPC.Mappers
{
    public static class CustomersModelsMapper
    {
        public static CustomerListItem MapToResponseListItem(this Customer customer)
        {
            return new CustomerListItem
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }

        public static CustomerResponse MapToCustomerResponse(this Customer customer)
        {
            var result = new CustomerResponse
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                StatusCode = StatusCodes.Status200OK
            };
            if (customer.Preferences != null && customer.Preferences.Any())
            {
                result.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponse
                {
                    Id = x.PreferenceId.ToString(),
                    Name = x.Preference?.Name
                }));
            }
            return result;
        }

        public static Customer MapToCustomerWithPreferences(this CreateCustomerRequest model, IEnumerable<Preference> preferences)
        {
            var customerId = Guid.NewGuid();
            return new Customer
            {
                Id = customerId,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Preferences = BuildPreferences(preferences, customerId)
            };
        }

        public static Customer UpdateWithPreferencesAndRequestData(this Customer customer,
            EditCustomerRequest request,
            IEnumerable<Preference> preferences)
        {
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = BuildPreferences(preferences, customer.Id);
            return customer;
        }

        private static List<CustomerPreference> BuildPreferences(IEnumerable<Preference> preferences, Guid customerId)
        {
            return preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customerId,
                PreferenceId = x.Id
            }).ToList();
        }
    }
}
