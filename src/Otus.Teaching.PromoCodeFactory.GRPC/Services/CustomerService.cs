﻿using Grpc.Core;
using Microsoft.AspNetCore.Http;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GRPC.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GRPC.Services
{
    public class CustomerService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<AllCustomersResponse> GetCustomers(EmptyRequest request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersList = customers.Select(x => x.MapToResponseListItem());

            var response = new AllCustomersResponse();
            response.Customers.AddRange(customersList);
            return response;
        }

        public override async Task<CustomerResponse> GetCustomer(RequestById request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var guid))
            {
                return new CustomerResponse {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Error = new Error { Message = "Invalid customer id" }
                };
            }

            var customer = await _customerRepository.GetByIdAsync(guid);           

            return customer?.MapToCustomerResponse()
                ?? new CustomerResponse { StatusCode = StatusCodes.Status404NotFound };
        }

        public override async Task<CustomerResponse> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            var ids = GetPreferencesGuids(request.PreferenceIds);
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(ids);

            Customer customer = request.MapToCustomerWithPreferences(preferences);
            await _customerRepository.AddAsync(customer);
            return await GetCustomer(new RequestById { Id = customer.Id.ToString() }, context);
        }

        public override async Task<StatusResponse> EditCustomer(EditCustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var guid))
            {
                return new StatusResponse
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Error = new Error { Message = "Invalid customer id" }
                };
            }

            var customer = await _customerRepository.GetByIdAsync(guid);
            if (customer == null)
            {
                return new StatusResponse { StatusCode = StatusCodes.Status404NotFound };
            }

            var ids = GetPreferencesGuids(request.PreferenceIds);
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(ids);

            customer = customer.UpdateWithPreferencesAndRequestData(request, preferences);

            await _customerRepository.UpdateAsync(customer);
            return new StatusResponse { StatusCode = StatusCodes.Status204NoContent };
        }

        public override async Task<StatusResponse> DeleteCustomer(RequestById request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var guid))
            {
                return new StatusResponse { StatusCode = StatusCodes.Status404NotFound };
            }

            var customer = await _customerRepository.GetByIdAsync(guid);
            if (customer == null)
            {
                return new StatusResponse { StatusCode = StatusCodes.Status404NotFound };
            }

            await _customerRepository.DeleteAsync(customer);
            return new StatusResponse { StatusCode = StatusCodes.Status204NoContent };
        }

        private List<Guid> GetPreferencesGuids(IEnumerable<string> preferencesIds)
        {
            return preferencesIds
                .Select(x => Guid.TryParse(x, out var id) ? (Guid?)id : null)
                .Where(x => x != null)
                .Select(x => x.Value)
                .ToList();
        }
    }
}
