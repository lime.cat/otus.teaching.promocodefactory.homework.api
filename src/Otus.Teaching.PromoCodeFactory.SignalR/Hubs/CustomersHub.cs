﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.SignalR.Models;
using Otus.Teaching.PromoCodeFactory.SignalR.Models.Settings;
using Otus.Teaching.PromoCodeFactory.SignalR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Hubs
{
    public class CustomersHub : Hub
    {
        public const string AttachMethod = "attach";
        public const string NotificationMethod = "notify";

        private static readonly string[] PossibleActions = new[]
        {
            nameof(ICustomerService.GetCustomers),
            nameof(ICustomerService.GetCustomer),
            nameof(ICustomerService.CreateCustomer),
            nameof(ICustomerService.EditCustomer),
            nameof(ICustomerService.DeleteCustomer),
        };

        private const string RefreshCustomersListAction = "RefreshCustomersList";

        private readonly ICustomerService _customerService;
        private readonly HubSettings _hubSettings;
        private readonly ILogger _logger;

        public CustomersHub(ICustomerService customerService,
            IOptions<HubSettings> options,
            ILogger<CustomersHub> logger)
        {
            _customerService = customerService;
            _hubSettings = options.Value;
            _logger = logger;
        }

        public async Task StartWatching(string userName, string[] watchForActions)
        {
            if (string.IsNullOrEmpty(userName))
            {
                await Clients.Caller.SendAsync(AttachMethod,
                    MessageResult("Attempt to login as anonimous user. Please enter your name", Status.Warning));
            }
            else
            {
                if (watchForActions != null && watchForActions.Any())
                {
                    foreach (var action in watchForActions.Where(x => PossibleActions.Contains(x)))
                    {
                        await Groups.AddToGroupAsync(Context.ConnectionId, action);
                    }
                }
                //все пользователи получают обновленный список пользователей
                await Groups.AddToGroupAsync(Context.ConnectionId, RefreshCustomersListAction);

                await Clients.Caller.SendAsync(AttachMethod, OkResult());
                await Clients.Others.SendAsync(NotificationMethod,
                    MessageResult($"Let's say 'Welcome' to {userName} and pray he/she won't break anything"));
            }
        }

        public async Task StoptWatching(string[] stopWatchingForActions)
        {
            if (stopWatchingForActions != null && stopWatchingForActions.Any())
            {
                foreach (var action in stopWatchingForActions.Where(x => PossibleActions.Contains(x)))
                {
                    await Groups.RemoveFromGroupAsync(Context.ConnectionId, action);
                }
                if (stopWatchingForActions.Contains(RefreshCustomersListAction))
                {
                    await Groups.RemoveFromGroupAsync(Context.ConnectionId, RefreshCustomersListAction);
                }
                await Clients.Caller.SendAsync(NotificationMethod,
                    MessageResult("Subscription on actions has been changed"));
            }
        }

        public async Task GetCustomers()
        {
            const string targetAction = nameof(ICustomerService.GetCustomers);
            IEnumerable<CustomerShortResponse> result;
            try
            {
                result = await _customerService.GetCustomers();
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync(NotificationMethod,
                    ErrorResult(targetAction, "Exception while getting customers"));
                return;
            }
            await Clients.Caller.SendAsync(NotificationMethod,
                DataResult(targetAction, result));
            await Clients.OthersInGroup(targetAction)
                    .SendAsync(NotificationMethod, MessageResult($"Customers list was requested"));
        }

        public async Task GetCustomer(string customerId)
        {
            const string targetAction = nameof(ICustomerService.GetCustomer);
            CustomerResponse result;
            try
            {
                result = await _customerService.GetCustomer(customerId);
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync(NotificationMethod,
                    ErrorResult(targetAction, $"Exception while getting customer {customerId}"));
                return;
            }
            var resultStatus = ResolveStatusByCode(result.StatusCode);
            await Clients.Caller.SendAsync(NotificationMethod,
                DataResult(targetAction, result, resultStatus));

            if (resultStatus == Status.Ok)
            {
                await Clients.OthersInGroup(targetAction)
                    .SendAsync(NotificationMethod, MessageResult($"Customer {customerId} was requested"));
            }
        }

        public async Task CreateCustomer(CreateOrEditCustomerRequest request)
        {
            const string targetAction = nameof(ICustomerService.CreateCustomer);
            CustomerResponse result;
            try
            {
                result = await _customerService.CreateCustomer(request);
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync(NotificationMethod,
                    ErrorResult(targetAction, "Exception while customer creation"));
                return;
            }

            var resultStatus = ResolveStatusByCode(result.StatusCode);
            await Clients.Caller.SendAsync(NotificationMethod,
                DataResult(targetAction, result, resultStatus));

            if (resultStatus == Status.Ok)
            {
                await Clients.OthersInGroup(targetAction)
                    .SendAsync(NotificationMethod, MessageResult($"New customer was created. Customer id: {result.Id}"));
                await RefreshCustomersListForClientsIfNeeded();
            }
        }

        public async Task EditCustomer(CreateOrEditCustomerRequest request)
        {
            const string targetAction = nameof(ICustomerService.EditCustomer);
            StatusResult result;
            try
            {
                result = await _customerService.EditCustomer(request);
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync(NotificationMethod,
                       ErrorResult(targetAction, $"Exception while editing customer {request.Id}"));
                return;
            }

            var resultStatus = ResolveStatusByCode(result.StatusCode);
            await Clients.Caller.SendAsync(NotificationMethod,
                DataResult(targetAction, result, resultStatus));

            if (resultStatus == Status.Ok)
            {
                await Clients.OthersInGroup(targetAction)
                    .SendAsync(NotificationMethod, MessageResult($"Customer {request.Id} was edited"));
                await RefreshCustomersListForClientsIfNeeded();
            }
        }

        public async Task DeleteCustomer(string customerId)
        {
            const string targetAction = nameof(ICustomerService.DeleteCustomer);
            StatusResult result;
            try
            {
                result = await _customerService.DeleteCustomer(customerId);
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync(NotificationMethod,
                    ErrorResult(targetAction, $"Exception while delete customer {customerId}"));
                return;
            }

            var resultStatus = ResolveStatusByCode(result.StatusCode);
            await Clients.Caller.SendAsync(NotificationMethod,
                DataResult(targetAction, result, resultStatus));

            if (resultStatus == Status.Ok)
            {
                await Clients.OthersInGroup(targetAction)
                    .SendAsync(NotificationMethod, MessageResult($"Customer {customerId} was removed"));
                await RefreshCustomersListForClientsIfNeeded();
            }
        }

        public Task RefreshCustomersListForClientsIfNeeded() => _hubSettings.UpdateCustomersListForClientsInBackground
            ? RefreshCustomersListForClients()
            : Task.CompletedTask;

        public async Task RefreshCustomersListForClients()
        {
            try
            {
                var result = await _customerService.GetCustomers();
                await Clients.Group(RefreshCustomersListAction)
                    .SendAsync(NotificationMethod, DataResult(RefreshCustomersListAction, result));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Attempt to refresh customers list failed");
                return;
            }
        }

        public OperationsResultNotification ErrorResult(string action, string errorMesage)
            => new OperationsResultNotification
            {
                Message = errorMesage,
                Action = action,
                Status = Status.Error
            };

        public OperationsResultNotification DataResult(string action, object data, Status status = default)
            => new OperationsResultNotification
            {
                Data = data,
                Action = action,
                Status = status
            };

        public OperationsResultNotification MessageResult(string message, Status status = default)
            => new OperationsResultNotification
            {
                Message = message,
                Status = status
            };

        public OperationsResultNotification OkResult() => new OperationsResultNotification();

        private int[] OkStatusCodes = new[] { StatusCodes.Status200OK, StatusCodes.Status204NoContent };
        public Status ResolveStatusByCode(int? code)
        {
            return !code.HasValue || OkStatusCodes.Contains(code.Value)
                ? Status.Ok
                : Status.Error;
        }

        public class OperationsResultNotification
        {
            public object Data { get; set; }
            public string Message { get; set; }
            public string Action { get; set; }
            public Status Status { get; set; }
        }

        public enum Status
        {
            Ok,
            Warning,
            Error
        }
    }
}
