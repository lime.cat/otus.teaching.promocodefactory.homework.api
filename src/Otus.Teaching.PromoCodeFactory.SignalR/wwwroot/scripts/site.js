﻿(function () {
    let isConnected;
    let currentCustomer;

    const hubConnection = new signalR.HubConnectionBuilder()
        .withUrl("/customers")
        .build();

    hubConnection.on("notify", ({ message, status, data, action }) => {
        if (!isConnected) return;
        console.log({ message, status, data, action });
        showNotification(message, !status);
        showData(data, action);
    });

    hubConnection.on("attach", (result) => {
        const isErrorResult = result.status > 0;
        isConnected = !isErrorResult;
        if (isConnected) {
            $('#actions-settings').hide();
            $('#top-action-buttons').show();
            hubConnection.invoke("GetCustomers");
        } else {
            showNotification(result.message, false);
        }
    });

    $('#connect').on('click', function () {
        const userName = $("#username").val();
        const watchForActions = $('#actions-settings [data-available-actions] input:checked').toArray().map(x => x.value);
        console.log(watchForActions);
        hubConnection.invoke("StartWatching", userName, watchForActions);
    });
    $('[data-action]').on('click', (e) => processButtonActionClick(e));

    hubConnection.start();

    function resolveHubAction(actionType) {
        switch (actionType) {
            case 'refresh':
                return 'GetCustomers';
            case 'details':
                return 'GetCustomer';
            case 'create':
                return 'CreateCustomer';
            case 'update':
                return 'EditCustomer';
            case 'remove':
                return 'DeleteCustomer';
        }
    }

    function hideOperationsPanel() {
        $('#operations-panel').hide();
    }

    function showOperationsPanel() {
        $('#operations-panel').show();
    }

    function showNotification(message, isOk) {
        if (message) {
            const className = isOk ? '' : 'error';
            const date = new Date();
            $('#operations-history').append(`<div class='${className}'>${date.getDate()}.${(date.getMonth() + 1)}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()} - ${message}<div>`);
        }
    }

    function showData(data, action) {
        switch (action) {
            case 'GetCustomers':
            case 'RefreshCustomersList':
                showCustomers(data);
                break;
            case 'GetCustomer':
                showCustomer(data);
                break;
        }
    }

    function showCustomers(data) {
        if (!data) {
            $("#customers-list").html('<div>Список пользователей пуст</div>');
        } else {
            const table = $('<table class="table"></table>');
            const rows = data.map((x) => buildCustomerDataRow(x));
            table.append(rows);
            $("#customers-list").html(table).prepend('<h6>Список пользователей</h6>');
            table.find('button[data-action]').on('click', processButtonActionClick);
        }
    }

    function processButtonActionClick(e) {
        const actionType = $(e.target).attr('data-action');
        const customerId = $(e.target).attr('data-customer-id');
        if (actionType == 'edit') {
            showEditForm(currentCustomer, 'update');
            return;
        }
        if (actionType == 'new-сustomer') {
            showEditForm({}, 'create');
            return;
        }
        const hubAction = resolveHubAction(actionType);
        hideOperationsPanel();
        if (actionType == 'refresh') {
            hubConnection.invoke(hubAction);
        } else if (['details', 'remove'].includes(actionType)) {
            hubConnection.invoke(hubAction, customerId);
        } else if (['update', 'create'].includes(actionType)) {
            const request = buildRequest(customerId);
            hubConnection.invoke(hubAction, request);
        }
    }

    function buildCustomerDataRow(customer) {
        const row = $('<tr></tr>');
        row.append(`<td>${customer.id}</td>`);
        row.append(`<td>${customer.firstName}</td>`);
        row.append(`<td>${customer.lastName}</td>`);
        row.append(`<td>${customer.email}</td>`);
        const buttonsCell = $('<td></td>');
        buttonsCell.append(`<button class="btn" data-customer-id='${customer.id}' data-action='details'>Детали</button>`);
        buttonsCell.append(`<button class="btn btn-danger" data-customer-id='${customer.id}' data-action='remove'>Удалить</button>`);
        row.append(buttonsCell);
        return row;
    }

    function showCustomer(data) {
        $('#operations-panel > div').hide();
        currentCustomer = data;
        const detailsBlock = $('#details-action').show();
        detailsBlock.find('[data-firstname]').text(data.firstName);
        detailsBlock.find('[data-lastname]').text(data.lastName);
        detailsBlock.find('[data-email]').text(data.email);
        if (data.preferences && data.preferences.length) {
            detailsBlock.find('[data-preferences]').text(data.preferences.map(x => x.name).join(', '));
        } else {
            detailsBlock.find('[data-preferences]').text('-');
        }
        detailsBlock.show();
        detailsBlock.find('button[data-action]').attr('data-customer-id', data.id);
        showOperationsPanel();
    }

    function showEditForm(customer, saveAction) {
        $('#operations-panel > div').hide();
        const form = $('#edit-action');
        form.find('[name="firstName"]').val(customer.firstName);
        form.find('[name="lastName"]').val(customer.lastName);
        form.find('[name="email"]').val(customer.email);
        form.find('[data-preferences] input').prop('checked', false);
        if (customer.preferences) {
            customer.preferences.map(x => x.id).forEach(x => {
                form.find(`[data-preferences] input[value="${x}"]`).prop('checked', true);
            });
        }
        form.find('[data-action]')
            .attr('data-action', saveAction)
            .attr('data-customer-id', customer.id);
        form.show();
    }

    function buildRequest(customerId) {
        const form = $('#edit-action');
        return ({
            id: customerId,
            firstName: form.find('[name="firstName"]').val(),
            lastName: form.find('[name="lastName"]').val(),
            email: form.find('[name="email"]').val(),
            preferenceIds: form.find('[data-preferences] input:checked').toArray().map(x => x.value)
        });
    }
})();