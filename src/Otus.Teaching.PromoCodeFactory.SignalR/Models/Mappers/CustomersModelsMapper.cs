﻿using Microsoft.AspNetCore.Http;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Models.Mappers
{
    public static class CustomersModelsMapper
    {
        public static CustomerShortResponse MapToResponseListItem(this Customer customer)
        {
            return new CustomerShortResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }

        public static CustomerResponse MapToCustomerResponse(this Customer customer)
        {
            var result = new CustomerResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                StatusCode = StatusCodes.Status200OK
            };
            if (customer.Preferences != null && customer.Preferences.Any())
            {
                var mappedPreferences = customer.Preferences.Select(x => new PreferenceResponse
                {
                    Id = x.PreferenceId,
                    Name = x.Preference?.Name
                });

                if(result.Preferences != null)
                {
                    result.Preferences.AddRange(mappedPreferences);
                }
                else
                {
                    result.Preferences = mappedPreferences.ToList();
                }                
            }
            return result;
        }

        public static Customer MapToCustomerWithPreferences(this CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences)
        {
            var customerId = Guid.NewGuid();
            return new Customer
            {
                Id = customerId,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Preferences = BuildPreferences(preferences, customerId)
            };
        }

        public static Customer UpdateWithRequestData(this Customer customer,
            CreateOrEditCustomerRequest request)
        {
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            return customer;
        }

        public static List<CustomerPreference> BuildPreferences(IEnumerable<Preference> preferences, Guid customerId)
        {
            return preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customerId,
                PreferenceId = x.Id
            }).ToList();
        }
    }
}
