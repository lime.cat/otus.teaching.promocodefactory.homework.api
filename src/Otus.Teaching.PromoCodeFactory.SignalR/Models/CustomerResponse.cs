﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Models
{
    public class CustomerResponse : StatusResult
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponse> Preferences { get; set; }
    }
}
