﻿namespace Otus.Teaching.PromoCodeFactory.SignalR.Models
{
    public class StatusResult
    {
        public int? StatusCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
