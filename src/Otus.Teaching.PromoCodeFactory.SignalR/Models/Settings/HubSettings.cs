﻿namespace Otus.Teaching.PromoCodeFactory.SignalR.Models.Settings
{
    public class HubSettings
    {
        public bool UpdateCustomersListForClientsInBackground { get; set; }
    }
}
