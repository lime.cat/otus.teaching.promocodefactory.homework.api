﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Models
{
    public class CreateOrEditCustomerRequest
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<string> PreferenceIds { get; set; }
    }
}
