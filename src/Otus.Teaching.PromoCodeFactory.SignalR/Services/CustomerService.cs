﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.SignalR.Models;
using Otus.Teaching.PromoCodeFactory.SignalR.Models.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly ILogger _logger;

        public CustomerService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            ILogger<CustomerService> logger)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _logger = logger;
        }

        public async Task<IEnumerable<CustomerShortResponse>> GetCustomers()
        {
            var customers = await _customerRepository.GetAllAsync();
            return customers.Select(x => x.MapToResponseListItem()).ToArray();
        }

        public async Task<CustomerResponse> GetCustomer(string customerId)
        {
            if (!Guid.TryParse(customerId, out var guid))
            {
                return new CustomerResponse
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    ErrorMessage = "Invalid customer id"
                };
            }

            var customer = await _customerRepository.GetByIdAsync(guid);

            return customer?.MapToCustomerResponse()
                ?? new CustomerResponse { StatusCode = StatusCodes.Status404NotFound };
        }

        public async Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request)
        {
            var ids = GetPreferencesGuids(request.PreferenceIds);
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(ids);

            Customer customer = request.MapToCustomerWithPreferences(preferences);
            await _customerRepository.AddAsync(customer);
            return (await _customerRepository.GetByIdAsync(customer.Id))?.MapToCustomerResponse();
        }

        public async Task<StatusResult> EditCustomer(CreateOrEditCustomerRequest request)
        {
            if (!Guid.TryParse(request.Id, out var guid))
            {
                return new StatusResult
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    ErrorMessage = "Invalid customer id"
                };
            }

            var customer = await _customerRepository.GetByIdAsync(guid);
            if (customer == null)
            {
                return new StatusResult { StatusCode = StatusCodes.Status404NotFound };
            }

            var ids = GetPreferencesGuids(request.PreferenceIds);
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(ids);
            try
            {
                customer.UpdateWithRequestData(request);
                if (customer.Preferences == null || customer.Preferences.Any())
                {
                    customer.Preferences = preferences.Select(x => BuildPreference(customer, x)).ToList();
                }
                else
                {
                    foreach(var itemForRemoving in customer.Preferences.Where(x => !ids.Contains(x.PreferenceId)))
                    {
                        customer.Preferences.Remove(itemForRemoving);
                    }
                    foreach(var preference in preferences.Where(p => !customer.Preferences.Any(x => x.PreferenceId == p.Id)))
                    {
                        customer.Preferences.Add(BuildPreference(customer, preference));
                    }
                }

                await _customerRepository.UpdateAsync(customer);
                return new StatusResult { StatusCode = StatusCodes.Status204NoContent };
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"Attempt to edit user '{guid}' failed");
                return new StatusResult { StatusCode = StatusCodes.Status500InternalServerError };
            }
        }

        public async Task<StatusResult> DeleteCustomer(string customerId)
        {
            if (!Guid.TryParse(customerId, out var guid))
            {
                return new StatusResult { StatusCode = StatusCodes.Status404NotFound };
            }

            var customer = await _customerRepository.GetByIdAsync(guid);
            if (customer == null)
            {
                return new StatusResult { StatusCode = StatusCodes.Status404NotFound };
            }

            await _customerRepository.DeleteAsync(customer);
            return new StatusResult { StatusCode = StatusCodes.Status204NoContent };
        }

        private List<Guid> GetPreferencesGuids(IEnumerable<string> preferencesIds)
        {
            return preferencesIds
                .Select(x => Guid.TryParse(x, out var id) ? (Guid?)id : null)
                .Where(x => x != null)
                .Select(x => x.Value)
                .ToList();
        }

        private CustomerPreference BuildPreference(Customer customer, Preference preference)
        {
            return new CustomerPreference
            {
                Preference = preference,
                PreferenceId = preference.Id,
                Customer = customer,
                CustomerId = customer.Id
            };
        }
    }
}
