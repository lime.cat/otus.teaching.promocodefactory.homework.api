﻿using Otus.Teaching.PromoCodeFactory.SignalR.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Services
{
    public interface ICustomerService
    {
        Task<IEnumerable<CustomerShortResponse>> GetCustomers();
        Task<CustomerResponse> GetCustomer(string customerId);
        Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request);
        Task<StatusResult> EditCustomer(CreateOrEditCustomerRequest request);
        Task<StatusResult> DeleteCustomer(string customerId);
    }
}
